#  sierra_quote_dialog.py
#
#  Copyright (C) 2019 Sierra Circuits, Inc
#
#  Designed by KiCad Services, Inc.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import os
import json

import wx

from . import sierra_order_GUI
from .sierra_order_GUI import quoteDialog

tip_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "images", "help-tip.png")

class Unit:
    INCHES = 0
    MM = 1

    @staticmethod
    def ToInches(value):
        return value / 2.54e+7

    @staticmethod
    def ToMil(value):
        return value / 2.54e+4
    
    @staticmethod
    def ToMM(value):
        return value / 1e+6

    @staticmethod
    def FromInches(value):
        return value * 2.54e+7

    @staticmethod
    def FromMil(value):
        return value * 2.54e+4
    
    @staticmethod
    def FromMM(value):
        return value * 1e+6


class QuoteDialog (sierra_order_GUI.quoteDialog):
    
    board_length = 0
    board_width = 0
    outer_min_trace_width = 0
    outer_min_trace_space = 0
    inner_min_trace_width = 0
    inner_min_trace_space = 0
    hole_count = 0
    hole_density = 0
    minimum_drill = 0
    minimum_ring = 0
    slot_count = 0

    min_drill = None
    min_ring = None
    min_drill_size = 0
    min_ring_size = 0

    def SetSizeHints(self, sz1, sz2):
        try:
            # wxPython 3
            self.SetSizeHintsSz(sz1, sz2)
        except TypeError:
            # wxPython 4
            super(QuoteDialog, self).SetSizeHints(sz1, sz2)
        
    def __init__(self, parent, logo_path):
        sierra_order_GUI.quoteDialog.__init__(self, parent)
        self.m_electricalTip.SetBitmap(wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG))
        self.m_electricalTip.SetToolTip(wx.ToolTip( "We provide 100% Electrical Netlist Testing of PCBs for an extra charge. You can decline testing for PCBs up to 6 layers. If your PCB has more than 6 layers, Electrical Testing is mandatory"))
        
        self.m_FinishThickTip.SetBitmap(wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG))
        self.m_FinishThickTip.SetToolTip(wx.ToolTip( "The total thickness of the board including all plating and final finishes"))
        
        self.m_FinishTypeTip.SetBitmap(wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG))
        self.m_FinishTypeTip.SetToolTip( wx.ToolTip( "Immersion Gold: 3-10 microinches of gold over electroless nickel (ENIG). This is also a lead-free finish" ) )

        self.m_HolesCountTip.SetBitmap(wx.Bitmap(tip_path, wx.BITMAP_TYPE_PNG))
        self.m_HolesCountTip.SetToolTip(wx.ToolTip("Total number of holes per board"))
        
        self.m_HolesDensityTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_HolesDensityTip.SetToolTip(wx.ToolTip("This is important because a board with too much hole density is complex and takes a lot of resources on the drilling machine. If your board has more than 80 holes/square inch on the average, you may not get an automatic quote on Web PCBS promotion, and a sales person may need to get involved."))
        
        self.m_HolesMinRingTip.SetBitmap(wx.Bitmap(tip_path, wx.BITMAP_TYPE_PNG))
        self.m_HolesMinRingTip.SetToolTip(wx.ToolTip("Smallest Annular Ring ( Pad Size - Hole Size / 2 )"))
        
        self.m_HolesMinSizeTip.SetBitmap(wx.Bitmap(tip_path, wx.BITMAP_TYPE_PNG))
        
        self.m_MaskColorTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_MaskFinishTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_MaskSidesTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_minInnerTraceSpaceTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_minInnerTraceWidthTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_minOuterTraceSpaceTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_minOuterTraceWidthTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_platingTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_RoHSTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_SilkColorTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_SilkSideTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_SilkTypeTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        self.m_VendorTip.SetBitmap( wx.Bitmap( tip_path, wx.BITMAP_TYPE_PNG ) )
        
        self.m_okCancelOK.SetLabelText("Get Quote")
        self.m_bpButton1.SetBitmap(wx.Bitmap(logo_path, wx.BITMAP_TYPE_PNG))
        self.Fit()
        width, discard = self.GetTextExtent('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        discard, height = self.GetSize()
        self.SetMaxSize((width, height))
        self.SetMinSize((width, height))
        self.SetSize((width, height))

        self.SetSize((width,-1))
    
    def onUpdateUI(self, event):
        try:
            self.CheckBoardSize(self.board_width)
        except ValueError as e:
            self.m_width.SetForegroundColour("RED")
            self.m_width.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        try:
            self.CheckBoardSize(self.board_length)
        except ValueError as e:
            self.m_length.SetForegroundColour("RED")
            self.m_length.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        try:
            self.CheckHoleDensity(self.hole_density)
        except ValueError as e:
            self.m_holeDensity.SetForegroundColour("RED")
            self.m_holeDensity.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        try:
            self.CheckMaxSlotCount(self.slot_count)
        except ValueError as e:
            self.m_cutoutCount.SetForegroundColour("RED")
            self.m_cutoutCount.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        try:
            self.CheckMaxHoleCount(self.hole_count)
        except ValueError as e:
            self.m_holeCount.SetForegroundColour("RED")
            self.m_holeCount.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        try:
            self.CheckMinHoleSize(self.min_drill_size)
        except ValueError as e:
            self.m_minHoleSize.SetForegroundColour("RED")
            self.m_minHoleSize.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        try:
            self.CheckMinRingSize(self.min_ring_size)
        except ValueError as e:
            self.m_minAnnularRing.SetForegroundColour("RED")
            self.m_minAnnularRing.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        try:
            self.CheckMinTrackSpace(self.outer_min_trace_space)
        except ValueError as e:
            self.m_outerMinSpace.SetForegroundColour("RED")
            self.m_outerMinSpace.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        try:
            self.CheckMinTrackWidth(self.outer_min_trace_width)
        except ValueError as e:
            self.m_outerMinTrace.SetForegroundColour("RED")
            self.m_outerMinTrace.SetToolTip(wx.ToolTip(str(e)))
            self.m_okCancelOK.Disable()

        if self.m_layerCount.GetSelection() > 0:
            try:
                self.CheckMinTrackSpace(self.inner_min_trace_space)
            except ValueError as e:
                self.m_innerMinSpace.SetForegroundColour("RED")
                self.m_innerMinSpace.SetToolTip(wx.ToolTip(str(e)))
                self.m_okCancelOK.Disable()

            try:
                self.CheckMinTrackWidth(self.inner_min_trace_width)
            except ValueError as e:
                self.m_innerMinTrace.SetForegroundColour('RED')
                self.m_innerMinTrace.SetToolTip(wx.ToolTip(str(e)))
                self.m_okCancelOK.Disable()


    def onSelectUnits(self, event):
        self.SetDimensions( self.board_length, self.board_width )
        event.Skip()

    def onChangeTraceUnits(self, event):
        self.UpdateTraceUnits()
        event.Skip()

    def UpdateTraceUnits(self):

        if( self.m_layerCount.GetSelection != 0 ):
            self.SetTrackWidth(self.m_innerMinTrace, self.m_innerMinTraceUnits, self.inner_min_trace_width)
            self.SetTrackWidth(self.m_innerMinSpace, self.m_innerMinSpaceUnits, self.inner_min_trace_space)
            self.m_innerMinTraceUnits.Enable()
            self.m_innerMinSpaceUnits.Enable()
        else:
            self.m_innerMinSpace.SetValue("")
            self.m_innerMinTrace.SetValue("")
            self.m_innerMinTraceUnits.Disable()
            self.m_innerMinSpaceUnits.Disable()
        
        self.SetTrackWidth(self.m_outerMinTrace, self.m_outerMinTraceUnits, self.outer_min_trace_width)
        self.SetTrackWidth(self.m_outerMinSpace, self.m_outerMinSpaceUnits, self.outer_min_trace_space)

    def SetDimensions(self, x, y):
        self.board_length = y
        self.board_width = x
        if( self.m_dimunits.GetSelection() == Unit.INCHES ):
            self.m_width.SetValue( '{0:.2f}'.format( Unit.ToInches( x ) ) )
            self.m_length.SetValue( '{0:.2f}'.format( Unit.ToInches( y ) ) )
        else:
            self.m_width.SetValue( '{0:.2f}'.format( Unit.ToMM( x ) ) )
            self.m_length.SetValue('{0:.2f}'.format(Unit.ToMM(y)))

    def SetTrackWidth( self, textfield, units, value ):
        if( units.GetSelection() == Unit.MM ):
            textfield.SetValue( '{0:.1f}'.format( Unit.ToMM( value ) ) )
        else:
            textfield.SetValue( '{0:.3f}'.format( Unit.ToMil( value ) ) )

    def SetDrill(self, drill, drill_size):
        self.min_drill = drill
        self.min_drill_size = drill_size
        self.m_minHoleSize.SetValue('{0:.2f}'.format(Unit.ToMM(drill_size)))

    def SetRing(self, ring, ring_size):
        self.min_ring = ring
        self.min_ring_size = ring_size
        self.m_minAnnularRing.SetValue('{0:.2f}'.format(Unit.ToMM(ring_size)))

    def CheckBoardSize(self, size):
        inch_size = Unit.ToInches(size)

        if inch_size > 15.9:
            raise ValueError('Board dimension %s too large (max 15.9 in)' % '{0:.2f}'.format(inch_size))
        elif inch_size < 0.5:
            raise ValueError('Board dimension %s too small (min 0.5 in)' % '{0:.2f}'.format(inch_size))
        
        return inch_size

    def CheckMinRingSize(self, size):
        inch_size = Unit.ToInches(size)

        if inch_size >= 0.007:
            return 0.007
        elif inch_size >= 0.006:
            return 0.006
        elif inch_size >= 0.005:
            return 0.005
        elif inch_size >= 0.004:
            return 0.004
        
        raise ValueError('Annular ring too small (min 4 mil)')

    def CheckMinTrackWidth(self, size):
        inch_size = Unit.ToInches(size)

        if inch_size >= 0.006:
            return 0.006
        elif inch_size >= 0.005:
            return 0.005
        elif inch_size >= 0.004:
            return 0.004
        
        raise ValueError('Track width too small (min 4 mil)')


    def CheckMinTrackSpace(self, size):
        inch_size = Unit.ToInches(size)

        if inch_size >= 0.006:
            return 0.006
        elif inch_size >= 0.005:
            return 0.005
        elif inch_size >= 0.004:
            return 0.004
        
        raise ValueError('Track spacing too small (min 4 mil)')

    def CheckMaxHoleCount(self, number):
        
        if number <= 400:
            return number
        
        raise ValueError('Too many holes (max 400)')

    def CheckMaxSlotCount(self, number):
        
        if number <= 10:
            return number
        
        raise ValueError('Too many slots (max 10)')

    def CheckMinHoleSize(self, size):
        inch_size = Unit.ToInches(size)

        if inch_size >= 0.01:
            return 0.01
        elif inch_size >= 0.008:
            return 0.008
        
        raise ValueError('Hole size too small (min 8 mil)')

    def CheckHoleDensity(self, density):
        
        inch_size = Unit.FromInches(Unit.FromInches(density)) #Convert from nm^-2 to in^-2

        if inch_size <= 80.0:
            return inch_size

        raise ValueError('Too many holes per sq inch (max 80 in^-2)')
        


    def GenerateJSON(self, board_name):
        if self.m_layerCount.GetSelection() > 0:
            min_space = min(self.inner_min_trace_space,self.outer_min_trace_space)
            min_trace = min(self.inner_min_trace_width, self.outer_min_trace_width)
        else:
            min_space = self.outer_min_trace_space
            min_trace = self.outer_min_trace_width

        min_trace = self.CheckMinTrackWidth(min_trace)
        min_space = self.CheckMinTrackSpace(min_space)
        min_hole = self.CheckMinHoleSize(self.min_drill_size)
        min_ring = self.CheckMinRingSize(self.min_ring_size)
        
        layer_string = "2 Layer"

        if self.m_layerCount.GetSelection() == 1:
            layer_string = "4 Layer"
        if self.m_layerCount.GetSelection() == 2:
            layer_string = "6 Layer"
        if self.m_layerCount.GetSelection() == 3:
            layer_string = "8 Layer"
        if self.m_layerCount.GetSelection() == 4:
            layer_string = "10 Layer"
            
        slots = self.m_plating.GetStringSelection()
        if slots is 'Both':
            slots = 'Both Plated and Non Plated'

        ret = json.dumps(
            {
                "indBoardXDim": self.m_width.GetValue(),
                "indBoardYDim": self.m_length.GetValue(),
                "layers": layer_string,
                "minHoleSize": '{0:.3f}'.format(min_hole) + " inches",
                "minimumSpace": '{0:.3f}'.format(min_space) + " inches",
                "minimumTrace": '{0:.3f}'.format(min_trace) + " inches",
                "minAnnularRing": '{0:.3f}'.format(min_ring) + " inches",
                "noOfHolesPerBoard": self.m_holeCount.GetValue(),
                "partNumber": board_name,
                "partRevision": "1",
                "slotsCutouts": slots,
                "productId": "2",
                "scTemplateId": "KICAD_STD"
            }
        )

        return ret

