#  action_sierra_quote.py
#
#  Copyright (C) 2019 Sierra Circuits, Inc
#
#  Designed by KiCad Services, Inc.
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import wx
import pcbnew
import logging
import os
import sys
import math
import ConfigParser
import json
import webbrowser
import ssl

# Big Warning flags here.
# We don't have a default way to find/utilize a known certificate list
# So we don't use certificates. :*(
ssl._create_default_https_context = ssl._create_unverified_context
from collections import defaultdict


VERSION = "1.0"
FORGOT_PASSWORD_LINK = "https://www.protoexpress.com/user/forgot_pass_mail.jsp"
CREATE_ACCOUNT_LINK = "https://www.protoexpress.com/chklogin.jsp"

logo_path = os.path.dirname(os.path.realpath(__file__)) + os.path.sep + "images" + os.path.sep + "logo.png"
config_path = pcbnew.GetKicadConfigPath()
config_file = os.path.join(config_path, "sierra_plugin.cfg")

class Unit:
    INCHES = 0
    MM = 1

    @staticmethod
    def ToInches(value):
        return value / 2.54e+7

    @staticmethod
    def ToMil(value):
        return value / 2.54e+4
    
    @staticmethod
    def ToMM(value):
        return value / 1e+6

    @staticmethod
    def FromInches(value):
        return value * 2.54e+7

    @staticmethod
    def FromMil(value):
        return value * 2.54e+4
    
    @staticmethod
    def FromMM(value):
        return value * 1e+6

# import gui and API module
from . import sierra_API
from .sierra_API import sierra_api
from . import generate_gerber_archive
from .generate_gerber_archive import GenerateProduction
from . import sierra_order_GUI
from .sierra_order_GUI import loginDialog, uploadDialog, matrixDialog

from . import sierra_quote_dialog
from .sierra_quote_dialog import QuoteDialog

class LoginDialog (sierra_order_GUI.loginDialog):
    def SetSizeHints(self, sz1, sz2):
        try:
            # wxPython 3
            self.SetSizeHintsSz(sz1, sz2)
        except TypeError:
            # wxPython 4
            super(LoginDialog, self).SetSizeHints(sz1, sz2)

    def ShowNetworkErrorDialog(self):
            self.ShowUnknownErrorDialog( "Network error trying to contact quote server" )

    def ShowLoginErrorDialog(self):
            self.ShowUnknownErrorDialog( "Username or password incorrect" )

    def ShowUnknownErrorDialog(self, msg):
            caption = 'Sierra Circuits Quote'
            dlg = wx.MessageDialog(self, msg, caption, wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()
            dlg.Destroy()

    def __init__(self, parent, api):
        sierra_order_GUI.loginDialog.__init__(self, parent)
        self.api = api
        self.SetDefaultItem( self.m_login )
        self.SetAffirmativeId( self.m_login.GetId() )
        self.SetEscapeId( wx.ID_CANCEL )
        self.m_bpButton1.SetBitmap(wx.Bitmap(logo_path, wx.BITMAP_TYPE_PNG))
        self.Fit()

        width, discard = self.GetTextExtent('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        discard, height = self.GetSize()
        self.SetMaxSize((width, height))
        self.SetMinSize((width, height))
        self.SetSize((width, height))

        if( api.RegisterPlugin() is not True ):
            if( self.api.last_error == self.api.NETWORK_ERROR ):
                self.ShowNetworkErrorDialog()
            elif( self.api.last_error == self.api.AUTH_ERROR ):
                self.ShowLoginErrorDialog()
            else:
                self.ShowUnknownErrorDialog( "Unknown Error" )

    def onForgotPassword(self, event):
        webbrowser.open( FORGOT_PASSWORD_LINK )
        event.Skip()

    def onCreateAccount(self, event):
        webbrowser.open( CREATE_ACCOUNT_LINK )
        event.Skip()

    def onLogin(self, event):
        if( self.api.registered is not True ):
            if( self.api.RegisterPlugin() is not True ):
                self.ShowNetworkErrorDialog()
                event.Skip( False )
                return

        self.api.SetUsername( self.m_username.GetValue() )
        self.api.SetPassword( self.m_password.GetValue() )

        try:
            self.api.Login()
        except Exception as e:
            self.ShowUnknownErrorDialog( str( e ) )
            return

        if( self.api.logged_in is not True ):
            if( self.api.last_error == self.api.NETWORK_ERROR ):
                self.ShowNetworkErrorDialog()
            elif( self.api.last_error == self.api.AUTH_ERROR ):
                self.ShowLoginErrorDialog()
            else:
                self.ShowUnknownErrorDialog( "Unknown Error" )
            
            event.Skip( False )
            return
        
        event.Skip()
        


class UploadDialog (sierra_order_GUI.uploadDialog):
    def SetSizeHints(self, sz1, sz2):
        try:
            # wxPython 3
            self.SetSizeHintsSz(sz1, sz2)
        except TypeError:
            # wxPython 4
            super(UploadDialog, self).SetSizeHints(sz1, sz2)
    def __init__(self, parent):
        sierra_order_GUI.uploadDialog.__init__(self, parent)
        self.m_bpButton1.SetBitmap(wx.Bitmap(logo_path, wx.BITMAP_TYPE_PNG))
        self.Fit()
        width, discard = self.GetTextExtent('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        discard, height = self.GetSize()
        self.SetMaxSize((width, height))
        self.SetMinSize((width, height))
        self.SetSize((width, height))


class MatrixDialog(sierra_order_GUI.matrixDialog):
    use_etest = 0
    unit_select = 11
    turn_list = ()
    qty_list = ()
    data = defaultdict(dict)
    wmqID = ""
    
    def SetSizeHints(self, sz1, sz2):
        try:
            # wxPython 3
            self.SetSizeHintsSz(sz1, sz2)
        except TypeError:
            # wxPython 4
            super(MatrixDialog, self).SetSizeHints(sz1, sz2)
    def __init__(self, parent):
        sierra_order_GUI.matrixDialog.__init__(self, parent)
        self.m_bpButton1.SetBitmap(wx.Bitmap(logo_path, wx.BITMAP_TYPE_PNG))
        # Hide the extra turn time elements
        self.m_turn2.Hide()
        self.m_qty21.Hide()
        self.m_qty22.Hide()
        self.m_qty23.Hide()
        self.m_qty24.Hide()
        self.m_turn3.Hide()
        self.m_qty31.Hide()
        self.m_qty32.Hide()
        self.m_qty33.Hide()
        self.m_qty34.Hide()
        self.m_turn4.Hide()
        self.m_qty41.Hide()
        self.m_qty42.Hide()
        self.m_qty43.Hide()
        self.m_qty44.Hide()

        self.Fit()
        width, discard = self.GetTextExtent('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
        height = width * 2 / 3 + self.m_bpButton1.GetBitmap().GetHeight()
        # discard, height = self.GetSize()
        self.SetMaxSize((width, height))
        self.SetMinSize((width, height))
        self.SetSize((width, height))

    def onSelect11(self, event):
        self.unit_select = 11
    def onSelect12(self, event):
        self.unit_select = 12
    def onSelect13(self, event):
        self.unit_select = 13
    def onSelect14(self, event):
        self.unit_select = 14
    def onSelect21(self, event):
        self.unit_select = 21
    def onSelect22(self, event):
        self.unit_select = 22
    def onSelect23(self, event):
        self.unit_select = 23
    def onSelect24(self, event):
        self.unit_select = 24
    def onSelect31(self, event):
        self.unit_select = 31
    def onSelect32(self, event):
        self.unit_select = 32
    def onSelect33(self, event):
        self.unit_select = 33
    def onSelect34(self, event):
        self.unit_select = 34
    def onSelect41(self, event):
        self.unit_select = 41
    def onSelect42(self, event):
        self.unit_select = 42
    def onSelect43(self, event):
        self.unit_select = 43
    def onSelect44(self, event):
        self.unit_select = 44

    def onUpdateUI(self, event):
        for i in range(1, 5):
            for j in range(1, 5):
                getattr(self, 'm_qty' + str(i) + str(j)).SetValue(False)

        qty = self.qty_list[self.unit_select % 10 - 1]
        turn = self.turn_list[self.unit_select / 10 - 1]
        getattr(self, 'm_qty' + str(self.unit_select)).SetValue(True)

        if self.use_etest:
            self.m_etest.SetLabelText('$' + str(self.data[qty][turn][1]))
        else:
            self.m_etest.SetLabelText('$0')

        event.Skip()
            
    def setReturnMatrix(self, retval):
        
        for item in retval['webQuoteMatrix']:
            try:
                qty = int(item['quantity'])
                days = int(item['turntime'])
                test = float(item['testingPrice'])
                unit = float(item['unitPrice'])
                iscall = bool(item['isCallUs'] != '0')
                
                # Only supporting 5-days right now
                if days == 5:
                    self.data[qty][days] = (iscall, test, unit)
            except:
                continue

        self.qty_list = list(self.data)
        self.qty_list.sort()

        if len(self.qty_list) < 4:
            self.m_qty4Text.Hide()
            self.m_qty14.Hide()
            self.m_qty24.Hide()
            self.m_qty34.Hide()
            self.m_qty44.Hide()
        if len(self.qty_list) < 3:
            self.m_qty3Text.Hide()
            self.m_qty13.Hide()
            self.m_qty23.Hide()
            self.m_qty33.Hide()
            self.m_qty43.Hide()
        if len(self.qty_list) < 2:
            self.m_qty2Text.Hide()
            self.m_qty12.Hide()
            self.m_qty22.Hide()
            self.m_qty32.Hide()
            self.m_qty42.Hide()

        try:
            self.m_qty1Text.SetLabelText(self.qty_list[0])
            self.m_qty1Text.SetLabelText(self.qty_list[1])
            self.m_qty1Text.SetLabelText(self.qty_list[2])
            self.m_qty1Text.SetLabelText(self.qty_list[3])
        except:
            pass

        
        self.turn_list = list(self.data[self.qty_list[0]])
        self.turn_list.sort()

        try:
            self.m_turn1.SetLabelText(str(self.turn_list[1]) + ' Days')
            self.m_turn2.SetLabelText(str(self.turn_list[1]) + ' Days')
            self.m_turn3.SetLabelText(str(self.turn_list[2]) + ' Days')
            self.m_turn4.SetLabelText(str(self.turn_list[3]) + ' Days')
        except:
            pass


        for i in range(len(self.qty_list)):
            # for j in range(len(self.turn_list)):
            j = 0
            try:
                qty = self.qty_list[i]
                turn = 5 # We only support 5-day turn time at the moment.  Otherwise, use self.turn_list[j]
                if self.data[qty][turn][0]:
                    getattr(self, 'm_qty' + str(j + 1) + str(i + 1)).SetLabelText('Call Us')
                    getattr(self, 'm_qty' + str(j + 1) + str(i + 1)).Disable()
                else:    
                    getattr(self, 'm_qty' + str(j + 1) + str(i + 1)).SetLabelText(str(self.data[qty][turn][2]))
            except:
                pass
            

        self.wmqID = retval['wmqID']

    def getWebData(self):
        return 'wmq_id=' + self.wmqID + '&pcb_quantity=' + str(self.qty_list[self.unit_select % 10 - 1])


class Segment:
    x1 = 0
    y1 = 0
    x2 = 0
    y2 = 0

    def __init__(self, track):
        self.x1 = track.GetStart().x
        self.y1 = track.GetStart().y
        self.x2 = track.GetEnd().x
        self.y2 = track.GetEnd().y

    def segments_intersect(self, other):
        """ whether two segments in the plane intersect
        """
        dx = self.x2 - self.x1
        dy = self.y2 - self.y1
        da = other.x2 - other.x1
        db = other.y2 - other.y1
        delta = float(da * dy - db * dx)
        if delta == 0.0:
            return False  # parallel segments

        s = (dx * (other.y1 - self.y1) + dy * (self.x1 - other.x1)) / delta
        t = (da * (self.y1 - other.y1) + db * (other.x1 - self.x1)) / (-delta)
        return (0 <= s <= 1) and (0 <= t <= 1)

    def point_segment_distance(self, px, py, x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        if dx == dy == 0:
            return math.hypot(px - x1, py - y1)

        t = ((px - x1) * dx + (py - y1) * dy) / (dx * dx + dy * dy)

        # Is this an endpoint or in the middle?
        if t < 0:
            dx = px - x1
            dy = py - y1
        elif t > 1:
            dx = px - x2
            dy = py - y2
        else:
            near_x = x1 + t * dx
            near_y = y1 + t * dy
            dx = px - near_x
            dy = py - near_y

        return math.hypot(dx, dy)

    def segments_distance(self, other):
        """ distance between two segments in the plane"""

        if self.segments_intersect(other):
            return 0

        # try each of the 4 vertices w/the other segment
        distances = []
        distances.append(self.point_segment_distance(self.x1, self.y1, other.x1, other.y1, other.x2, other.y2))
        distances.append(self.point_segment_distance(self.x2, self.y2, other.x1, other.y1, other.x2, other.y2))
        distances.append(self.point_segment_distance(other.x1, other.y1, self.x1, self.y1, self.x2, self.y2))
        distances.append(self.point_segment_distance(other.x2, other.y2, self.x1, self.y1, self.x2, self.y2))

        return min(distances)

class RequestQuote(pcbnew.ActionPlugin):
    """
    An action plugin to request online quotes from KiCad
    """

    def defaults(self):
        self.name = "Sierra Circuits Quote"
        self.category = "Quoting"
        self.description = "Request an online quote from Sierra Circuits"
        self.icon_file_name = os.path.join(
            os.path.dirname(__file__), 'sierra_icon24.png')

    def GetBoardDims(self, outlines):

        outline = outlines.Outline(0)
        outline_points = [outline.Point(n) for n in range(outline.PointCount())]
        outline_maxx = max(map(lambda p: p.x, outline_points))
        outline_minx = min(map(lambda p: p.x, outline_points))
        outline_maxy = max(map(lambda p: p.y, outline_points))
        outline_miny = min(map(lambda p: p.y, outline_points))

        return( outline_maxx - outline_minx, outline_maxy - outline_miny )

    def GetMinOuterTraceWidth( self, board ):

        traces = defaultdict(list)

        for track in board.GetTracks():
            if( track.GetLayer() == pcbnew.F_Cu or track.GetLayer() == pcbnew.B_Cu ):
                traces[ str( track.GetWidth() ).rjust( 12, '0' ) ].append( track )

        if( len( traces ) > 0 ):
            return sorted(traces.items())[0][1]
        
        return None

    def GetMinInnerTraceWidth( self, board ):

        traces = defaultdict(list)

        for track in board.GetTracks():
            if( track.GetLayer() != pcbnew.F_Cu and track.GetLayer() != pcbnew.B_Cu ):
                traces[ str( track.GetWidth() ).rjust( 12, '0' ) ].append( track )

        if( len( traces ) > 0 ):
            return sorted(traces.items())[0][1]
        
        return None

    def GetMinSpace( self, board ):
        inner_traces = None
        inner_trace_space = sys.maxsize
        outer_traces = None
        outer_trace_space = sys.maxsize

        from itertools import combinations
        for track1, track2 in combinations(board.GetTracks(), 2):
            if track1.GetNetCode() == track2.GetNetCode():
                continue
            if track1.GetLayer() != track2.GetLayer():
                continue
            
            if (track1.Type() == pcbnew.PCB_TRACE_T and track2.Type() == pcbnew.PCB_TRACE_T):
                seg1 = Segment(track1)
                seg2 = Segment(track2)
                dist = seg1.segments_distance(seg2) - track1.GetWidth() / 2 - track2.GetWidth() / 2
                
                if dist <= 0:
                    print( "Track1: %d %d -> %d %d, Track2: %d %d -> %d %d" % ( track1.GetStart().x, track1.GetStart().y, track1.GetEnd().x, track1.GetEnd().y, track2.GetStart().x, track2.GetStart().y, track2.GetEnd().x, track2.GetEnd().y))
                    track1.SetHighlighted()
                    track2.SetHighlighted()
                
                if ((track1.GetLayer() == pcbnew.F_Cu or track1.GetLayer() == pcbnew.B_Cu)
                        and dist < outer_trace_space):
                    outer_trace_space = dist
                    outer_traces = (track1, track2)

                if ((track1.GetLayer() != pcbnew.F_Cu and track1.GetLayer() != pcbnew.B_Cu)
                        and dist < inner_trace_space):
                    inner_trace_space = dist
                    inner_traces = (track1, track2)
        
        pcbnew.Refresh()
        try:
            pcbnew.UpdateUserInterface()
        except:
            pass

        return inner_traces, inner_trace_space, outer_traces, outer_trace_space

    
    def GetDrills(self, board):
        min_drill_size = sys.maxsize
        min_ring_size = sys.maxsize
        min_drill = 0
        min_ring = 0
        total_drills = 0

        for track in board.GetTracks():
            if (track.Type() == pcbnew.PCB_VIA_T):
                total_drills = total_drills + 1
                if (track.GetDrillValue() < min_drill_size):
                    min_drill_size = track.GetDrillValue()
                    min_drill = track
                
                ring = (track.GetWidth() - track.GetDrillValue()) / 2
                if (ring < min_ring_size):
                    min_ring_size = ring
                    min_ring = track
                    
        # Note: Should we separate the drill-drag from the route command?
        for pad in board.GetPads():
            size = pad.GetDrillSize()
            min_size = min([size.x, size.y])
            max_size = max([size.x, size.y])

            if (pad.GetAttribute() == pcbnew.PAD_ATTRIB_HOLE_NOT_PLATED or pad.GetAttribute() == pcbnew.PAD_ATTRIB_STANDARD):
                total_drills = total_drills + 1
                if (min_size < min_drill_size):
                    min_drill_size = min_size
                    min_drill = pad


            if (pad.GetAttribute() == pcbnew.PAD_ATTRIB_STANDARD):
                poly = pcbnew.SHAPE_POLY_SET()
                inflate = pcbnew.wxSize(0,0)
                pad.BuildPadShapePolygon(poly, inflate, 16, 1.0 / math.cos(math.pi / 16))
                outline = poly.COutline(0)
                # Note: This does potentially over estimate oblong drill hits
                d = outline.Distance(pcbnew.VECTOR2I(pad.GetOffset()), True) - max_size / 2
                if (d < min_ring_size):
                    min_ring_size = d
                    min_ring = pad                    
        
        return min_drill, min_ring, min_drill_size, min_ring_size, total_drills

    def GetCutouts( self, board ):

        outlines = pcbnew.SHAPE_POLY_SET()
        return outlines.HoleCount( 0 )

    def Run(self):

        # load board
        board = pcbnew.GetBoard()
        outlines = pcbnew.SHAPE_POLY_SET()
        board.GetBoardPolygonOutlines(outlines, "")
        config = ConfigParser.ConfigParser(allow_no_value=True)

        # go to the project folder - so that log will be in proper place
        board_path = os.path.dirname( os.path.realpath( board.GetFileName() ) ) + os.path.sep

        # Remove all handlers associated with the root logger object.
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)

        # set up logger
        try:
            logging.basicConfig(level=logging.DEBUG,
                                filename= board_path + "sierra_quote.log",
                                filemode='w',
                                format='%(asctime)s %(name)s %(lineno)d:%(message)s',
                                datefmt='%m-%d %H:%M:%S')
        except:
            # If we can't write to the file, just use STDERR
            logging.basicConfig(level=logging.DEBUG,
                                format='%(asctime)s %(name)s %(lineno)d:%(message)s',
                                datefmt='%m-%d %H:%M:%S')

        logger = logging.getLogger(__name__)
        logger.info("Sierra Circuits Quote plugin version: " + VERSION + " started")


        stdout_logger = logging.getLogger('STDOUT')
        sl = StreamToLogger(stdout_logger, logging.INFO)
        sys.stdout = sl

        stderr_logger = logging.getLogger('STDERR')
        sl = StreamToLogger(stderr_logger, logging.ERROR)
        sys.stderr = sl

        # Load the configuration
        config.read(config_file)

        # Init the API 
        api = sierra_api(logger)
        
        try:
            api.username = config.get("MAIN","username")
        except:
            pass

        try:
            api.refresh_token = config.get("MAIN", "refresh")
        except:
            pass

        try:
            api.uuid = config.get("MAIN", "uuid")
        except:
            pass

        # find pcbnew frame
        _pcbnew_frame = [x for x in wx.GetTopLevelWindows() if x.GetTitle().lower().startswith('pcbnew')][0]
        
        # try to get back without logging in
        if not api.RefreshPlugin(api.refresh_token):
            # show login dialog

            main_dialog = LoginDialog(_pcbnew_frame, api)
            
            if (api.username is not ""):
                main_dialog.m_username.SetValue(api.username)
                main_dialog.m_password.SetFocus()

            main_res = main_dialog.ShowModal()

            if main_res != main_dialog.m_login.GetId():
                main_dialog.Destroy()
                logging.shutdown()
                return
            
            main_dialog.Destroy()

        # We successfully logged in. Let's save the last username
        try:
            config.add_section("MAIN")
        except:
            pass

        try:
            config.set("MAIN", "username", api.username)
            config.set("MAIN", "refresh", api.refresh_token)
            config.set("MAIN", "uuid", api.uuid)

            with open(config_file, 'wb') as configfile:
                config.write(configfile)
        except:
            logger.info("Could not save config file " + config_file)
            pass


        quote_dialog = QuoteDialog(_pcbnew_frame, logo_path)
        
        design_settings = board.GetDesignSettings()
        layers = design_settings.GetCopperLayerCount()
        if layers == 2:
            quote_dialog.m_layerCount.SetSelection(0)
        elif layers == 4:
            quote_dialog.m_layerCount.SetSelection(1)
        elif layers == 6:
            quote_dialog.m_layerCount.SetSelection(3)
            quote_dialog.m_NetlistTesting.SetValue(True)
            quote_dialog.m_NetlistTesting.Disable()
        elif layers == 8:
            quote_dialog.m_layerCount.SetSelection(4)
            quote_dialog.m_NetlistTesting.SetValue(True)
            quote_dialog.m_NetlistTesting.Disable()
        elif layers == 10:
            quote_dialog.m_layerCount.SetSelection(6)
            quote_dialog.m_NetlistTesting.SetValue(True)
            quote_dialog.m_NetlistTesting.Disable()
        elif layers == 12:
            quote_dialog.m_layerCount.SetSelection(8)
            quote_dialog.m_NetlistTesting.SetValue(True)
            quote_dialog.m_NetlistTesting.Disable()
        else:
            caption = 'Sierra Circuits Quote'
            msg = 'Invalid layer count %d.  We currently only handle 2, 4, 6, 8, 10 or 12 layers.' % layers
            dlg = wx.MessageDialog(_pcbnew_frame, msg, caption, wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            quote_dialog.Destroy()
            logging.shutdown()
            return

        quote_dialog.m_layerCount.Disable()

        outer_trace = self.GetMinOuterTraceWidth(board)
        inner_trace = self.GetMinInnerTraceWidth(board)
        min_inner_traces, min_inner_trace_space, min_outer_traces, min_outer_trace_space = self.GetMinSpace(board)
        drill, ring, drill_size, ring_size, total_drills = self.GetDrills(board)

        quote_dialog.SetRing(ring, ring_size)
        quote_dialog.SetDrill(drill, drill_size)
        if inner_trace is not None:
            quote_dialog.inner_min_trace_width = inner_trace[0].GetWidth()
            quote_dialog.inner_min_trace_space = min_inner_trace_space
        
        quote_dialog.outer_min_trace_width = outer_trace[0].GetWidth()
        quote_dialog.outer_min_trace_space = min_outer_trace_space
        quote_dialog.UpdateTraceUnits()
                
        quote_dialog.m_holeCount.SetValue(str(total_drills))
        quote_dialog.hole_density = total_drills / outlines.COutline(0).Area()

        density = total_drills / ( outlines.COutline(0).Area() / (6.452e+14) ) # Scale to in^-2
        quote_dialog.m_holeDensity.SetValue( '{0:.2f}'.format( density ) + " drills / in^2")

        if outlines.OutlineCount > 1:
            quote_dialog.m_cutoutCount.SetValue( str(outlines.OutlineCount() - 1) )
            quote_dialog.m_plating.Enable()
        else:
            quote_dialog.m_cutoutCount.SetValue("0")
            quote_dialog.m_plating.Disable()

        quote_dialog.SetDimensions(*self.GetBoardDims(outlines))
        
        success_data = False
        name, ext = os.path.splitext(os.path.basename(board.GetFileName()))

        while not success_data:
            if quote_dialog.ShowModal() != wx.ID_OK:
                quote_dialog.Destroy()
                logging.shutdown()
                return

            try:
                json_data = quote_dialog.GenerateJSON(name)
            except ValueError as e:
                caption = 'Sierra Circuits Quote'
                msg = str(e)
                dlg = wx.MessageDialog(_pcbnew_frame, msg, caption, wx.OK | wx.ICON_ERROR)
                dlg.ShowModal()
                dlg.Destroy()
            else:
                success_data = True

        needs_etest = quote_dialog.m_NetlistTesting.GetValue()
        quote_dialog.Destroy()

        retval = api.GetMatrix(json_data)
        if retval is None:
            caption = 'Sierra Circuits Quote'
            msg = 'Problem getting quote: ' + api.last_error_message
            dlg = wx.MessageDialog(_pcbnew_frame, msg, caption, wx.OK | wx.ICON_ERROR)
            dlg.ShowModal()
            dlg.Destroy()
            logging.shutdown()
            return

        matrix_dialog = MatrixDialog(_pcbnew_frame)
        matrix_dialog.use_etest = needs_etest
        matrix_dialog.setReturnMatrix(retval)
        if matrix_dialog.ShowModal() != wx.ID_OK:
            matrix_dialog.Destroy()
            logging.shutdown()
            return
        
        gen = GenerateProduction(board)
        gen.archive_project()

        try:
            redir = api.GetRedirect(matrix_dialog.getWebData())
            if redir is not None:
                webbrowser.open_new(redir)
        except:
            pass


        matrix_dialog.Destroy()
        logging.shutdown()

        pass


class StreamToLogger(object):
    """
    Fake file-like stream object that redirects writes to a logger instance.
    Based on GPL code by Ferry Boender https://electricmonk.nl/log
    """
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''

    def write(self, buf):
        try:
            for line in buf.rstrip().splitlines():
                self.logger.log(self.log_level, line.rstrip())
        # If we can't write to the log file, ignore
        except:
            pass

    def flush(self, *args, **kwargs):
        """No-op for wrapper"""
        pass
